//
//  login_items.h
//  login
//
//  Created by Samuel Inniss on 9/19/13.
//  Copyright (c) 2013 Samuel Inniss. All rights reserved.
//

#ifndef __login__login_items__
#define __login__login_items__

#include <iostream>

class login_items
{
    public:
        void SetUsername(const char string[], int length);
        //postcondition: "username" of object array is set to entered string
        void SetPassword(const char string[], int length);
        //postcondition: "password" of object array is set to entered string
        void PrintUsername();
        //prints "username" to iostream
        void PrintPassword();
        //prints "password" to iostream
        int GetArrLength();

    
    private:
        int arrleng = 15;
        char username[15];
        char password[15];
        int nameLength;
        int passLength;
};

#endif /* defined(__login__login_items__) */
