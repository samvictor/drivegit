//
//  main.cpp
//  login
//
//  Created by Samuel Inniss on 9/19/13.
//  Copyright (c) 2013 Samuel Inniss. All rights reserved.
//

#include <iostream>
#include "login_items.h"

int main()
{
    using std::cout;
    using std::cin;
    
    login_items newitem;
    int arrleng = newitem.GetArrLength();
    char username[arrleng], password[arrleng], confusername[arrleng], confpassword[arrleng];
    
    
    cout << "Hello, World!\nEnter a username followed by \".\"\n";
    
    int ulength = -1;
    do
    {
        if(ulength > arrleng-1)
        {
            cout << "Error: username must be less than " << arrleng << " characters!";
            return 0;
        }
        
        cin >> username[++ulength];
        
    }
    while((username[ulength] != '.'));
    
    newitem.SetUsername(username,++ulength);
    
    
    cout << "Enter a password followed by \".\"\n";
    
    ulength = -1;
    do
    {
        if(ulength > arrleng-1)
        {
            cout << "Error: password must be less than " << arrleng << " characters!";
            return 0;
        }
        
        cin >> password[++ulength];
    }
    while((password[ulength] != '.'));
    
    newitem.SetPassword(password,++ulength);

    newitem.PrintUsername();
    newitem.PrintPassword();
    
    return 0;
}

