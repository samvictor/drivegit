//
//  login_items.cpp
//  login
//
//  Created by Samuel Inniss on 9/19/13.
//  Copyright (c) 2013 Samuel Inniss. All rights reserved.
//

#include "login_items.h"

void login_items::SetUsername(const char name[], int length)
{
    nameLength = length;
    
    while(length > 0)
    {
        length--;
        username[length] = name[length];
    }
}

void login_items::SetPassword(const char pass[], int length)
{
    passLength = length;
    
    while(length > 0)
    {
        length--;
        password[length] = pass[length];
    }
}


void login_items::PrintUsername()
{
    std::cout << "Your username is ";
    for(int j = 0; j<nameLength-1; j++)
        std::cout << username[j];
    std::cout << std::endl;
}

void login_items::PrintPassword()
{
    std::cout << "Your password is ";
    for(int j = 0; j<passLength-1; j++)
        std::cout << password[j];
    std::cout << std::endl;
}

int login_items::GetArrLength()
{
    return arrleng;
}
fish

